<section>
    <div class="container pt-3">
        <div class="row">
            <% if $WishList.Products.count %>
            <% loop $WishList.Products %>
                <div class="col-sm-4">
                    <% include ProductBox Product=$ShowcaseProduct %>
                </div>
            <% end_loop %>
            <% else %>
                <p class="pt-5 pb-5">You have no products in your wish list.</p>
            <% end_if %>
        </div>
    </div>
</section>
