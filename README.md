# Instructions

## Home Page
### Featured Collections
Add the tag `featured` to the collection, and ensure 
`Home: Featured Collection Icon` is configured under
`Theme Elements` in these collections.

#### IMPORTANT: You should have 4 or 8 featured collections

---

### Promoted Products
4 products will be shown from the collection selected as per
`HomePromotedCollection`.

#### NOTE #1: Section will not show if collection doesn't have 4 products
#### NOTE #2: Title and subtitle are taken from the collection title and description fields respectively
#### NOTE #3: Products with variants are not supported here

### Our Projects
This section will automatically load the latest blog posts, and show the image
assigned to that blog post in a 640px x 720px canvas.

## Contact Page
### Form validation & spam protection
These are features native to Lifeboat and no additional action is required.

### Google Maps Embed
To show the google maps embed you need to;
1. Go to https://www.google.com/maps
2. Search for the location you want to embed
3. Click on `Share` > `Embed a map`
4. Copy the **src** parameter **only**
5. Paste it in the field `Design` > `Customise` > `Contact Page: Google Maps Embed Src`

## General
### Footer
In the address you can add `<br>` to force a new line where you want the address to break.